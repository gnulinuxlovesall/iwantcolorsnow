import colored

def interpret(command):
    color = {'b': 27, 'g': 46, 'y': 226, 'r': 1, 'w': 15, 'e': 238, 'k': 0, 'p': 201}
    ansi = ''
    for c in command:
        try:
            if c.isupper():
                ansi += colored.bg(color[c.lower()])
                # flipper for now
                if c.lower() == 'w':# or c.lower() == 'g' or c.lower() == 'y':
                    ansi += colored.fg(color['k'])
                if c.lower() == 'g' or c.lower() == 'y':
                    ansi += colored.fg(color['k'])
            elif c.islower():
                ansi += colored.fg(color[c])
                if c.lower() == 'g' or c.lower() == 'y':
                    ansi += colored.bg(color['e'])
            elif c == 1:
                ansi += colored.attr(1)
            elif c == 4:
                ansi += colored.attr(4)
        except KeyError:
            pass
    return ansi

def make_boxes(s):
    s = s.replace('#0', ' ')
    s = s.replace('$0', ' ')
    s = s.replace('#1', '!K_▁_')
    s = s.replace('#2', '!K_▂_')
    s = s.replace('#3', '!K_▃_')
    s = s.replace('#4', '!K_▄_')
    s = s.replace('#5', '!K_▅_')
    s = s.replace('#6', '!K_▆_')
    s = s.replace('#7', '!K_▇_')
    s = s.replace('#8', '!K_█_')
    s = s.replace('$1', '!W_▇_')
    s = s.replace('$2', '!W_▆_')
    s = s.replace('$3', '!W_▅_')
    s = s.replace('$4', '!W_▄_')
    s = s.replace('$5', '!W_▃_')
    s = s.replace('$6', '!W_▂_')
    s = s.replace('$7', '!W_▁_')
    s = s.replace('$8', '!W_ _')

    s = s.replace('#▖', '!K_▖_')
    s = s.replace('#▗', '!K_▗_')
    s = s.replace('#▘', '!K_▘_')
    s = s.replace('#▙', '!K_▙_')
    s = s.replace('#▚', '!K_▚_')
    s = s.replace('#▛', '!K_▛_')
    s = s.replace('#▜', '!K_▜_')
    s = s.replace('#▝', '!K_▝_')
    s = s.replace('#▞', '!K_▞_')
    s = s.replace('#▟', '!K_▟_')
    s = s.replace('$▖', '!W_▖_')
    s = s.replace('$▗', '!W_▗_')
    s = s.replace('$▘', '!W_▘_')
    s = s.replace('$▙', '!W_▙_')
    s = s.replace('$▚', '!W_▚_')
    s = s.replace('$▛', '!W_▛_')
    s = s.replace('$▜', '!W_▜_')
    s = s.replace('$▝', '!W_▝_')
    s = s.replace('$▞', '!W_▞_')
    s = s.replace('$▟', '!W_▟_')
    return s


def main():

    files = [
    "clrs/kitefretboard.clrs",
    "clrs/12edo_fretboard.clrs",
    "clrs/lattice.clrs",
    "clrs/41edo_5_string_chart.clrs",
    "clrs/keyboard.clrs",
    "clrs/qwerty",
    "clrs/12_and_41edo_strings_chart.clrs",
    "clrs/41edo_strings_chart.clrs",
    "clrs/chess_keyboard.clrs"]

    #files = ['lattice']
    s = ''
    for fname in files:
        f = open(fname,'r')
        s += '# ' + fname + '\n'
        s += f.read()
    s = make_boxes(s)
    l = []
    command = ''
    reading = False
    executing = False
    isP = False
    for c in s:
        if reading:
            if c != '_': # Reading
                command += c
                if c == 'P':
                    isP = True
            else:
                l += interpret(command)
                executing = True
                reading = False
                command = ''
        elif executing:
            if c != '_':
                # unicode arrows instead of v^
                if not isP:
                    if c == '^':
                        l += 'ˆ'
                    elif c == 'v':
                        l += 'ᵥ'
                    else:
                        l += c
                else:
                    l += c
            else:
                executing = False
                l += colored.attr(0)
                isP = False
                #l += colored.attr(0) + colored.attr(1)
        elif c == '!':
            reading = True
        else:
            l += c

    print(colored.attr(0))
    print(colored.fg('white'))
    print(''.join(l))

    # Depends on xterm
    # This puts the terminal into a page for the browser to load
    termhtml = open("term2.html",'r')
    s = termhtml.read()
    f = open("term.html",'w')

    before = """<!doctype html>
      <html>
        <head>
          <link rel="stylesheet" href="node_modules/xterm/css/xterm.css" />
          <script src="node_modules/xterm/lib/xterm.js"></script>
        </head>
        <body>
          <div id="terminal"></div>
          <script>
            var term = new Terminal();
            term.open(document.getElementById('terminal'));"""
    after = """      </script>
        </body>
      </html>
    """
    #f.write(s.replace("""term.write('Hello from \x1B[1;3;31mxterm.js\x1B[0m $ ')""","term.write('"+''.join(l)+')'))
    f.write(before + "term.write('" + (''.join(l)).replace('\n','\\r\\n')+')' + after)


if __name__ == "__main__":
    main()
