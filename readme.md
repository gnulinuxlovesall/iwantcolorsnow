# iwantcolorsnow

Markdown is **awesome**, and so are colors. I want both! Now!

This is my parser inspired by markdown.

## syntax

```
    . Exclamation starts reading codes
    |  . Underscore ends the code read
    v  v
```
```
    !Gb_  Capital G is green background _
    !Gb_  Lowercase b is blue forground _
```
```
  The last underscore resets to default ^
```


## run
```
pip3 install colored
python3 iwantcolorsnow.py
```

## browser display

This used to work for me but isn't right now.
For browser display run
```
npm install xterm
```
